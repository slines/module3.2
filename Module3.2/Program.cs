using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Module3_2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world!");
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            Int32.TryParse(input, out result);
            if (result >= 0)
                return Int32.TryParse(input, out result);
            return false;
        }

        public int[] GetFibonacciSequence(int n)
        {
            int[] fib_arr = new int[n];
            for (int i = 0; i < n; i++)
                fib_arr[i] = i;
            for (int i = 2; i < n; i++)
                fib_arr[i] = fib_arr[i - 2] + fib_arr[i - 1];
            return fib_arr;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            int new_number = 0;
            int len = Math.Abs(sourceNumber).ToString().Length;
            int count = 1;
            while (Math.Abs(sourceNumber) > 0)
            {
                new_number += Math.Abs(sourceNumber) % 10 * (int)Math.Pow(10, len - count);
                sourceNumber /= 10;
                count++;
            }
            if (sourceNumber < 0)
                new_number *= -1;
            return new_number;
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            Random rnd = new Random();
            int[] arr;
            try
            {
                arr = new int[size];
            }
            catch
            {
                arr = new int[0];
            }
            for (int i = 0; i < size; i++)
                arr[i] = rnd.Next(Int32.MinValue, Int32.MaxValue);
            return arr;
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            for (int i = 0; i < source.Length; i++)
                source[i] *= -1;
            return source;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            Random rnd = new Random();
            int[] arr;
            try
            {
                arr = new int[size];
            }
            catch
            {
                arr = new int[0];
            }
            for (int i = 0; i < size; i++)
                arr[i] = rnd.Next(-10, 10);
            return arr;
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> numbers = new List<int>();
            for (int i = 0; i < source.Length - 1; i++)
                if (source[i + 1] > source[i])
                    numbers.Add(source[i + 1]);
            return numbers;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            int[,] arr = new int[size, size];
            int n = size, m = size, c = 0, i = 0, j = 0, k = 0;
            while (k < size * size)
            {
                for (j = c; j < m; j++)
                {
                    if (k == size * size)
                        break;
                    arr[i, j] = k + 1;
                    k++;
                }
                j--;
                for (i = c + 1; i < n; i++)
                {
                    if (k == size * size)
                        break;
                    arr[i, j] = k + 1;
                    k++;
                }
                i--;
                for (j = m - 2; j >= c; j--)
                {
                    if (k == size * size)
                        break;
                    arr[i, j] = k + 1;
                    k++;
                }
                j++;
                for (i = n - 2; i >= c + 1; i--)
                {
                    if (k == size * size)
                        break;
                    arr[i, j] = k + 1;
                    k++;
                }
                c++;
                n--;
                m--;
                i = c;
            }
            return arr;
        }
    }
}
